Ohjelma Comiqin testauksen kesäharjoitteluun hakemista varten.

*"Tehtävänanto:*

*Tee noppapeli jollakin seuraavista olio-ohjelmointikielistä:*

*Java*

*C#*

*Python*

*C++*

*Ruby*

*
Pelin kulku:*

*1) Arvaa kahden nopan yhteenlaskettu silmäluku*

*2) Heitä noppaa*

*3) Näytä kahden nopan yhteenlaskettu tulos ja ilmoita pelaajalle osuiko arvaus oikein*

*4) Aloita uusi kierros tai lopeta ohjelma*

*Tekstipohjainen peli riittää, mutta voit myös tehdä UI:n halutessasi."*

Tekijä:

Anssi Oinonen

A.T.Oinonen@gmail.com