//Noppapeli versio 2.0, 14.4.2016
//Anssi Oinonen, A.T.Oinonen@gmail.com

//Peli kysyy pelaajalta luvun, heitää kahta noppaa,
//ja tarkistaa osuiko pelaajan arvaus oikeaan.

#include <iostream>
#include <string>
#include <ctime>

//Tarkistaa onko vastaus 'k' tai 'e'.
bool jarjellinenKirjain(char vastaus){
    if (vastaus != 'k' && vastaus != 'e'){
        std::cout << "Virheellinen vastaus." << std::endl
                  << "Anna vastaus joko kirjaimella 'k' tai 'e'."
                  << std::endl;
        return false;
    }else{
        return true;
    }
}

//Kysyy käyttäjältä jatkuuko peli ja
//palauttaa sen mukaisen totuusarvon.
bool jatkuuko(){
    std::string vastaus;
    char kirjain = ' ';

    //Kysyy "Haluatko jatkaa..." niin pitkään, kunnes käyttäjä vastaa
    //joko k tai e (tai toinen näistä on vastauksen ensimmäinen kirjain):
    do{
        vastaus.clear();
        std::cout << "Haluatko jatkaa, k (kylla), e (ei): ";
        std::getline(std::cin, vastaus);

        //Sijoitaa rivin ensimmäisen kirjaimen "kirjain" muuttujaan.
        for(std::size_t i = 0; i < vastaus.size(); ++i){
            if (!isspace (vastaus.at(i))){
                kirjain = vastaus.at(i);
                break;
            }
        }
    }while (!jarjellinenKirjain(kirjain));

    if (kirjain == 'k'){
        std::cout << "Jatketaan..." << std::endl;
        return true;
    }else{ //jos tänne mennään, "kirjain" on 'e'
        std::cout << "Lopetetaan." << std::endl;
        return false;
    }
}

//Palauttaa luvun 1 ja 6 välillä.
int heitaNoppaa(){
    int nopan_luku = std::rand()%6;
    return ++nopan_luku;
}

//Pyytää käyttäjää arvaamaan luvun.
int arvaaLuku(){
    std::string rivi;
    int arvattu_luku = 0;

    //while-loopia jatketaan niin pitkään kunnes saadaan int-luku
    while (true){
        rivi.clear();

        std::cout << "Arvaa kahden nopan yhteenlaskettu silmaluku (2-12): ";

        std::getline(std::cin, rivi);

        //try-lohko stoi-funktiota varten
        try{
            arvattu_luku = std::stoi(rivi);
        } catch (const std::invalid_argument& e){
            std::cout << "Lukua ei pystytty lukemaan. Anna luku 2 ja 12 valilta."
                      << std::endl;
            continue;
        } catch (const std::out_of_range& e){
            std::cout << "Annettu luku liian iso. Arvaa luku 2 ja 12 valilta."
                      << std::endl;
            continue;
        }
        //Jos ei saada poikkeusta stoi-funktiosta, lähdetään pois while-loopista.
        break;
    }
    return arvattu_luku;
}

int main(){

    //Alustaa std::rand()-funktion kellonajalla.
    std::srand(std::time(0));

    do{
        int arvattu_luku = arvaaLuku();
        int nopan_tulos = heitaNoppaa() + heitaNoppaa();
        std::cout << "Arvasit luvun: " << arvattu_luku << std::endl;
        std::cout << "Noppien yhteenlaskettu tulos: " << nopan_tulos << std::endl;

        if (nopan_tulos == arvattu_luku){
            std::cout << "Onnea, arvasit oikein!" << std::endl;
        }else{
            std::cout << "Valitettavasti arvauksesi meni pieleen..." << std::endl;
        }
    }while (jatkuuko());

    return EXIT_SUCCESS;
}
